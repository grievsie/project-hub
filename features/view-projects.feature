Feature: View projects

     Our team should be able easily see just our Git software projects.
     They are currently mixed up with hundreds of others in Orahub, across
     multiple groups.

Scenario: View our projects
     Given our team have Orahub Git projects
     When we go to the projects page
     Then we should see all of our projects
